_tsname=call TFAR_fnc_getTeamSpeakServerName;
_servername="育碧-土豆服务器";
if !(_tsname ==_servername) then{endMission "TS";};

execVM "scripts\pilotCheck.sqf";

#define SAFETY_ZONES [["safezone", 80]]
#define MESSAGE "注意：基地内部禁止使用武器"

if (isDedicated) exitWith {};
waitUntil {!isNull player};

//安全区
player addEventHandler ["Fired", {
    if ({(_this select 0) distance getMarkerPos (_x select 0) < _x select 1} count SAFETY_ZONES > 0) then
    {
        deleteVehicle (_this select 6);
        titleText [MESSAGE, "PLAIN", 3];
    };
}];